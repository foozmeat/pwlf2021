# Portland Winter Light Festival 2021

This project will drive a 1D light display for the festival and beyond.

### Installation

Development is being done on a [Teensy 3.6](https://www.pjrc.com/store/teensy36.html) board using [PlatformIO](https://platformio.org). Ensure that your LED strip is configured correctly in src/main.cpp. Next install the PlatformIO tools using [their instructions](https://docs.platformio.org/en/latest/core/installation/index.html) and then run

    platformio run --target upload -e teensy36

### Project

The physical installation consists of 1 strip of NeoPixels running along 2 walls at a 90º angle. There are 46 LEDs, a center LED, and 73 more LEDS.

### Acknowledgements

* The awesome [FastLED](http://fastled.io) library
* Some code adapted from https://www.tweaking4all.com/hardware/arduino/adruino-led-strip-effects/
* A few effects from https://gist.github.com/kriegsman
