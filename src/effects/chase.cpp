//
// Created by James Moore on 1/19/21.
//

#include "chase.h"
#include "lib/shared.h"

const int FRAMES_PER_SECOND = 10;

void chase() {
    int pix;
    int position = 0;

    resetStopwatch();

    while (stopwatch < 5000) {

        updateTimers();

        for (int i = EAST_START; i >= 0; i = i - 3) {
            pix = i - position;
            if (pix >= 0) {
                strip.setPixelColor(pix, 0, 0, 0);

            }
        }

        for (int i = SOUTH_START; i <= SOUTH_END; i = i + 3) {
            pix = i + position;
            if (pix <= SOUTH_END) {
                strip.setPixelColor(pix, 0, 0, 0);
            }
        }

        position++;
        if (position == 3) {
            position = 0;
        }

        for (int i = EAST_START; i >= 0; i = i - 3) {
            pix = i - position;
            if (pix >= 0) {
                strip.setPixelColor(pix, 255, 0, 0);
            }
        }

        for (int i = SOUTH_START; i <= SOUTH_END; i = i + 3) {
            pix = i + position;
            if (pix <= SOUTH_END) {
                strip.setPixelColor(pix, 0, 0, 255);
            }
        }

        strip.show();

        delay(1000 / FRAMES_PER_SECOND);

    }

    fadeOut(20);

}