//
// Created by bnf on 2021-02-05
//

// In support of Thomas Hudson's wall
// https://editor.p5js.org/hydronics2/present/YYg9urd-v
//
// A part of 2021 Portland Winter Light Festival
// https://www.pdxwlf.com/
//
// though hopefully this wall will bring great joy to many
// in our neighborhood for years to come
//
// AtoB
//
// Two random pixels are chosen (A and B)
// A moves to B's position
// with each step, change color to be closer to Bs
// a new random B pixel is chosen
// A moves to B's position...

#include "AtoB.h"
#include "lib/shared.h"

// const int PACE = 5;
// const int ALEN = 5;  // the length of A's tail
int Apos = 0;
int Bpos = 0;

uint16_t Ahue;
uint16_t Bhue;

void AtoB(int duration) {
    set_first_leds();
    resetStopwatch();

    Apos = randomPos();
    Ahue = random16();

    strip.setPixelColor(Apos, gamma_hsv(Ahue));

    while (stopwatch < duration * 1000) {
        updateTimers();
        int pace = random(25);

        Bpos = randomPos();
        Bhue = random16();
        strip.setPixelColor(Bpos, gamma_hsv(Bhue));
        strip.show();
        delay(pace);  // walking pace

        // foreach step EXCEPT for the last step
        int steps = abs(Apos - Bpos) - 1;
        int hueStep = abs(Ahue - Bhue) / steps;

        for (int i = 0; i < steps; i++) {
            // or maybe don't zero them out and just leave it messy
            strip.setPixelColor(Apos, 0, 0, 0);
            Ahue = moveTowards(Ahue, Bhue, hueStep);
            Apos = moveTowards(Apos, Bpos, 1);
            strip.setPixelColor(Apos, gamma_hsv(Ahue));

            // this tail doesn't really look right
            // for (int j = 1; j < ALEN && j < i; j++) {
            //     if (Apos < Bpos) {
            //         leds[Apos - j] = CHSV(Ahue, 255 / j, 255 / j);
            //     } else {
            //         leds[Apos + j] = CHSV(Ahue, 255 / j, 255 / j);
            //     }
            // }

            strip.show();
            delay(pace);  // walking pace
        }

        // for the last step, set to Bhue to accentuate the transition from
        // Ahue to Bhue
        strip.setPixelColor(Apos, 0, 0 ,0);
        Apos = moveTowards(Apos, Bpos, 1);
        strip.setPixelColor(Apos, gamma_hsv(Bhue));
        strip.show();
        delay(pace);  // walking pace
    }
    fadeOut(20);
}

// move A closer to B
uint16_t moveTowards(int A, int B, int increment) {
    // don't overshoot the target
    if (abs(A - B) < increment) {
        increment = abs(A - B);
    }

    if (A > B) {
        return A - increment;
    }
    return A + increment;
}

int randomPos() {
    int p = random8(SOUTH_END);
    if (p == Apos || p == Bpos || p < EAST_END) {
        p = randomPos();
    }
    return p;
}
