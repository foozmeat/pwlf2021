//
// Created by James Moore on 2/7/21.
//

#include "blocks.h"
#include "lib/shared.h"

void color_blocks(int duration) {

    const int FRAMES_PER_SECOND = 40;
    const uint16_t ring_length = duration * FRAMES_PER_SECOND;
    uint16_t color_ring[ring_length];
    uint16_t hue = random16();
    uint8_t color_change_chance = lround(255 * 0.2);
    uint16_t color_n = 0;

    // zero out the color ring
    for (uint16_t i = 0; i < ring_length; i++) {
        color_ring[i] = 0;
    }

    set_first_leds();

    resetStopwatch();
    while (stopwatch < duration * 1000) {
        updateTimers();

        uint16_t ring_pos = color_n % ring_length; // make sure we can't overflow the array
        if (random8() < color_change_chance) {
            hue = random16();
        }
        color_ring[ring_pos] = hue;
        // Serial.printf("color_n: %d, ring_pos: %d, hue: %d\n", color_n, ring_pos, hue);

        for (uint8_t i = EAST_END; i < SOUTH_END; i++) {
            if (ring_pos - i >= 0) {
                strip.setPixelColor(i, gamma_hsv(color_ring[ring_pos - i]));

            }
        }
        color_n++;

        strip.show();
        delay(1000 / FRAMES_PER_SECOND);

    }
    fadeOut(20);
}
