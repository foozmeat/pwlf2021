//
// Source: https://gist.github.com/kriegsman/062e10f7f07ba8518af6
//

#include "small.h"
#include "lib/shared.h"


void confetti(int duration) {

    set_first_leds();
    resetStopwatch();
    while (stopwatch < duration * 1000) {

        updateTimers();

        // random colored speckles that blink in and fade smoothly
        for (int i = EAST_END; i < SOUTH_END; i++) {
            uint32_t color = fadeToBlackBy(strip.getPixelColor(i), random8(150,255)/256.0);
            strip.setPixelColor(i, color);
        }
        int pos = random8(SOUTH_END);
        if (pos >= EAST_END) {
            strip.setPixelColor(pos, gamma_hsv(random16()));
        }
        strip.show();
        delay(1000 / 50);
    }
    fadeOut(20);

}

void juggle(int duration) {

    set_first_leds();
    resetStopwatch();
    while (stopwatch < duration * 1000) {

        updateTimers();

        for (int i = EAST_END; i < SOUTH_END; i++) {
            uint32_t color = fadeToBlackBy(strip.getPixelColor(i), 0.85);
            strip.setPixelColor(i, color);
        }

        // eight colored dots, weaving in and out of sync with each other
        uint16_t dothue = 0;
        for (int i = 0; i < 8; i++) {
            strip.setPixelColor(beatsin16(i + 7, EAST_END, SOUTH_END - 1), gamma_hsv(dothue));
            dothue += 8192;
        }
        strip.show();
    }
    fadeOut(20);

}

void rainbow_chase(int duration) {
    int length = 12;
    int segments = (SOUTH_END / length) + 1;
    uint16_t hue_offset = 0;

    set_first_leds();
    resetStopwatch();
    while (stopwatch < duration * 1000) {
        updateTimers();

        for (int i = 0; i < length; i++) {

            for (int i = EAST_END; i < SOUTH_END; i++) {
                uint32_t color = fadeToBlackBy(strip.getPixelColor(i), 0.50);
                strip.setPixelColor(i, color);
            }

            for (int s = 0; s < segments; s++) {
                float p = map((float) i, 0.0, (float) length - 1, 0.0, 1.0);
                float eased_p = LinearInterpolation(p);
                int eased_i = lround(map(eased_p, 0.0, 1.0, 0, length - 1));

                uint16_t hue = map(eased_i + (s * length), 0, SOUTH_END, 0, 65535);

                int pos = eased_i + (s * length);
                if (EAST_END <= pos && pos < SOUTH_END) {
                    strip.setPixelColor(pos, gamma_hsv(hue - hue_offset));
                }
//                Serial.printf("%i\t%f\t%f\t%i\n", i, p, eased_p, eased_i);
            }
            hue_offset += 65535 / SOUTH_END;
            strip.show();
            delay(1000 / 20);
        }

//        Serial.println("-----");
        strip.show();
    }
    fadeOut(20);

}

void rainbow_waves(int duration) {
    int wavelength = 12;
    int offset = 0;

    set_first_leds();
    resetStopwatch();
    while (stopwatch < duration * 1000) {
        updateTimers();

        offset += 255;

        for (int w = EAST_END; w < SOUTH_END; w++) {
            int p = map(w % wavelength, 0, wavelength - 1, 0, 255);
            uint16_t h = offset % 65535;
            // leds[w] = CHSV(h, 255, cos8(p + offset));
            strip.setPixelColor(w, strip.gamma32(strip.ColorHSV(h, 255, cos8(p + offset)))); 
        }
        strip.show();
        delay(1000 / 60);
    }
    fadeOut(20);

}
