//
// Created by James Moore on 1/21/21.
//

#include "meteor.h"
#include "lib/shared.h"

const int FRAMES_PER_SECOND = 30;

void meteorRain(int max_loops) {

    set_first_leds();

    for (int loops = 0; loops <= max_loops; loops++) {

        float meteorTrailDecay = random8(0, 100) / 256.0;
        int meteorSize = 5;
        int num_frames = CENTER;
        int draw_pos;

        uint16_t e_hue = random16();
        uint16_t s_hue = wrap16(e_hue + 8192);


        for (int frame = 0; frame < num_frames; frame++) {
            updateTimers();
            random16_add_entropy(random());

            // map the frame number into 0,1 space
            float p = map((float)frame, 0.0f, (float) num_frames, 0, 1);
//            if (p > 0.992) break; // cut off the long tail of the exponetial curve

            // apply easing
            float eased_p = QuinticEaseOut(p);

            // map back to position
            int e_pix = map(eased_p, 0, 1.0, EAST_START, -meteorSize);
            int s_pix = map(eased_p, 0, 1.0, SOUTH_START, SOUTH_END + meteorSize);

            // Create a random fading trail
            for (int j = EAST_END; j < SOUTH_END; j++) {
                if (random8(10) > 7) {
                    uint32_t new_color = fadeToBlackBy(strip.getPixelColor(j), meteorTrailDecay);
                    strip.setPixelColor(j, new_color);
                }
            }

            for (int j = meteorSize; j >= 0; j--) {
                draw_pos = e_pix - j;
                if (draw_pos <= EAST_START && draw_pos >= EAST_END) {
                    strip.setPixelColor(draw_pos, gamma_hsv(e_hue));
                }
            }

            for (int j = 0; j < meteorSize; j++) {
                draw_pos = s_pix - j;
                if (draw_pos < SOUTH_END && draw_pos >= SOUTH_START) {
                    strip.setPixelColor(draw_pos, gamma_hsv(s_hue));
                }
            }

            strip.show();
            delay(1000 / FRAMES_PER_SECOND);

        }
    }

    fadeOut(20);
}