//
// Created by James Moore on 1/27/21.
//

#include "planets.h"
#include "lib/shared.h"

const int FRAMES_PER_SECOND = 50;
const float length = SOUTH_END * 2.0f;
const int max_width = 7;

float wrap_pos(float pos) {
    if (pos > length) pos -= length;
    else if (pos < 0) pos += length;

    return pos;
}

void drawBar(float pos, uint32_t color, int width) {
    for (int pix = 0; pix < width; pix++) {
        float adj_pos = wrap_pos(pos + pix);
        if ((adj_pos >= EAST_END) && (adj_pos <= SOUTH_END)) {
            uint32_t old_color = strip.getPixelColor(lround(pos + pix));

            strip.setPixelColor(lround(pos + pix), old_color | color);
        }
    }
}

float pos_map(float pos) {

    pos = wrap_pos(pos);

    // adjust the position forward so the center ot the easing lands near the strip CENTER
    float adj = CENTER - 3.0;

    float f_result;

    if (pos <= SOUTH_END) {
        float p = map(pos, (float) EAST_END, (float) SOUTH_END, 0, 1);
        float eased_p = QuadraticEaseIn(p);
        f_result = map(eased_p, 0.0, 1.0, 0, SOUTH_END);
    } else {
        float p = map(pos, (float) SOUTH_END + 1, length, 0, 1);
        float eased_p = QuadraticEaseOut(p);
        f_result = map(eased_p, 0.0, 1.0, SOUTH_END + 1, length);
    }

    // adjust the new postion forward so the easiing is tied to CENTER
    float result = wrap_pos(f_result + adj);

//    Serial.printf("In Pos: %i\tP: %f\tEased P:%f\tf_result: %f\tresult: %i\n",
//                  pos, p, eased_p, f_result, result );
    return result;
}

int width_map(float pos) {
    pos = wrap_pos(pos);
    int width;
    int offset = max_width / 2;

    if (pos <= CENTER - offset) {
        width = map(pos, EAST_END, CENTER - offset, 1, max_width);
    } else  if (pos >= CENTER + offset) {
        width = map(pos, CENTER + offset, SOUTH_END, max_width, 1);
        if (width < 1) width = 1;
    } else {
        width = max_width;
    }

    return width;
}

void planets(int duration) {
    set_first_leds();
    resetStopwatch();

    float p1_pos = 0.0;
    float p1_v = float_rand(2.0, 2.5);
    uint32_t p1_color = gamma_hsv(RED);

    float p2_pos = 0.0;
    float p2_v = float_rand(1.5, 2.0);
    uint32_t p2_color = gamma_hsv(ORANGE);

    float p3_pos = 0.0; 
    float p3_v = float_rand(1.0, 1.5);
    uint32_t p3_color = gamma_hsv(BLUE);

    float p4_pos = 0.0;
    float p4_v = float_rand(0.5, 1.0);
    uint32_t p4_color = gamma_hsv(GREEN);

    while (stopwatch < duration * 1000) {

        updateTimers();
        set_first_leds();

        // draw sun
        strip.setPixelColor(CENTER, gamma_hsv(YELLOW));
 
        float eased_p1_pos = pos_map(p1_pos);
        float eased_p2_pos = pos_map(p2_pos);
        float eased_p3_pos = pos_map(p3_pos);
        float eased_p4_pos = pos_map(p4_pos);

        int p1_width = width_map(eased_p1_pos);
        int p2_width = width_map(eased_p2_pos);
        int p3_width = width_map(eased_p3_pos);
        int p4_width = width_map(eased_p4_pos);

        // Serial.printf("pos: %f\teased: %f\tw: %i\n", p1_pos, eased_p1_pos, p1_width);

        drawBar(eased_p1_pos, p1_color, p1_width);
        drawBar(eased_p2_pos, p2_color, p2_width);
        drawBar(eased_p3_pos, p3_color, p3_width);
        drawBar(eased_p4_pos, p4_color, p4_width);

        p1_pos = wrap_pos(p1_pos + p1_v);
        p2_pos = wrap_pos(p2_pos + p2_v);
        p3_pos = wrap_pos(p3_pos + p3_v);
        p4_pos = wrap_pos(p4_pos + p4_v);

        strip.show();
        delay(1000 / FRAMES_PER_SECOND);

    }

    fadeOut(20);

}
