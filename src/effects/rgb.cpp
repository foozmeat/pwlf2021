//
// Created by James Moore on 1/24/21.
//

#include "rgb.h"
#include "lib/shared.h"

const int run_time = 10000;

void rgb() {
    resetStopwatch();

    while (stopwatch < run_time) {
        updateTimers();

        for (int pix = 0; pix < 40; pix++) {
            strip.setPixelColor(pix, 255, 0, 0);

        }
        for (int pix = 40; pix < 80; pix++) {
            strip.setPixelColor(pix, 0, 255, 0);
        }
        for (int pix = 80; pix < 120; pix++) {
            strip.setPixelColor(pix, 0, 0, 255);
        }

        strip.show();
    }

    fadeOut(20);

}