//
// Created by James Moore on 1/26/21.
//

#include "countdown.h"
#include "lib/shared.h"

const int FRAMES_PER_SECOND = 60;
const int digit_width = 3;

void countdown(int duration) {
    set_first_leds();
    resetStopwatch();

    // lights up
    strip.fill(strip.Color(0, 0, 0, 255), EAST_END, SOUTH_END);
    strip.show();
    delay(2000);

    // rainbow cycle
    for (int i = EAST_END; i < SOUTH_END; i++) {
        uint16_t hue = (i * 65536) / SOUTH_END;

        strip.setPixelColor(i, gamma_hsv(hue));
        Serial.printf("%i: %i", i, hue);

        if (hue % 8192 == 0) {
            Serial.print("****");
        }
        Serial.println();

        strip.show();
        delay(1000 / 100);

    }

    int hue_offset = 0;
    resetStopwatch();
    while (stopwatch < duration * 1000) {
        updateTimers();
        for (int i = EAST_END; i < SOUTH_END; i++) {
            int hue = map(i + hue_offset, 0, SOUTH_END, 0, 65535);
            strip.setPixelColor(i, gamma_hsv(hue));
        }
        hue_offset--;
        delay(1000 / 100);
        strip.show();
    }

    fadeOut(30);
}
