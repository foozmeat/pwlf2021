// Fire2012 by Mark Kriegsman, July 2012
// as part of "Five Elements" shown here: http://youtu.be/knWiGsmgycY
//
// This basic one-dimensional 'fire' simulation works roughly as follows:
// There's a underlying array of 'heat' cells, that model the temperature
// at each point along the line.  Every cycle through the simulation,
// four steps are performed:
//  1) All cells cool down a little bit, losing heat to the air
//  2) The heat from each cell drifts 'up' and diffuses a little
//  3) Sometimes randomly new 'sparks' of heat are added at the bottom
//  4) The heat from each cell is rendered as a color into the leds array
//     The heat-to-color mapping uses a black-body radiation approximation.
//
// Temperature is in arbitrary units from 0 (cold black) to 255 (white hot).
//
// This simulation scales it self a bit depending on NUM_LEDS; it should look
// "OK" on anywhere from 20 to 100 LEDs without too much tweaking.
//
// I recommend running this simulation at anywhere from 30-100 frames per second,
// meaning an interframe delay of about 10-35 milliseconds.
//
//
// There are two main parameters you can play with to control the look and
// feel of your fire: COOLING (used in step 1 above), and SPARKING (used
// in step 3 above).
//
// COOLING: How much does the air cool as it rises?
// Less cooling = taller flames.  More cooling = shorter flames.
// Default 55, suggested range 20-100
#define COOLING  80

// SPARKING: What chance (out of 255) is there that a new spark will be lit?
// Higher chance = more roaring fire.  Lower chance = more flickery fire.
// Default 120, suggested range 50-200.
#define SPARKING 120

#include "fire.h"
#include "lib/shared.h"

static byte heat[_NUM_LEDS];
const int FRAMES_PER_SECOND = 20;

uint32_t MyHeatColor( uint8_t temperature) {
    uint8_t r, g, b;

    // Scale 'heat' down from 0-255 to 0-191,
    // which can then be easily divided into three
    // equal 'thirds' of 64 units each.
    uint8_t t192 = scale8_video( temperature, 191);

    // calculate a value that ramps up from
    // zero to 255 in each 'third' of the scale.
    uint8_t heatramp = t192 & 0x3F; // 0..63
    heatramp <<= 2; // scale up to 0..252
    int orange_point = 50;

    // now figure out which third of the spectrum we're in:
    if( t192 & 0x80) {
        // we're in the hottest third
        r = 255; // full red
        g = 255; // full green
        b = heatramp; // ramp up blue

    } else if( t192 & 0x40 ) {
        // we're in the middle third
        r = 255; // full red
        g = map(heatramp, 0, 252, orange_point, 252); // ramp up green
        b = 0; // no blue

    } else {
        // we're in the coolest third
        r = heatramp; // ramp up red
        g = map(heatramp, 0, 252, 0, orange_point); // add a little green to keep it orange
        b = 0; // no blue
    }

    uint32_t color = strip.Color(r, g, b);
    return color;
}

void Fire(int duration) {

    set_first_leds();

    resetStopwatch();

    while (stopwatch < duration * 1000) {
        updateTimers();

        // Step 1.  Cool down every cell a little
        for (int i = EAST_END; i < SOUTH_END; i++) {
            heat[i] = qsub8(heat[i], random8(0, ((COOLING * 10) / SOUTH_END) + 2));
        }

        // Step 2.  Heat from each cell drifts 'up' and diffuses a little
        for (int k = 0; k < CENTER; k++) {
            heat[k] = (heat[k + 1] + heat[k + 2] + heat[k + 2]) / 3;
        }

        for (int k = SOUTH_END; k > CENTER; k--) {
            heat[k] = (heat[k - 1] + heat[k - 2] + heat[k - 2]) / 3;
        }

        // Step 3.  Randomly ignite new 'sparks' of heat near the bottom
        if (stopwatch < duration * 1000) {

            if (random8() < SPARKING) {
                int y = random8(EAST_START - 7, EAST_START);
                heat[y] = qadd8(heat[y], random8(160, 255));
            }

            if (random8() < SPARKING) {
                int y = random8(SOUTH_START, SOUTH_START + 7);
                heat[y] = qadd8(heat[y], random8(160, 255));
            }
            strip.setPixelColor(CENTER, MyHeatColor(random8(160, 255)));

        }

        // Step 4.  Map from heat cells to LED colors
        for (int j = EAST_END; j < SOUTH_END; j++) {
            strip.setPixelColor(j, MyHeatColor(heat[j]));
        }

        strip.show();
        delay(1000 / FRAMES_PER_SECOND);
    }

    fadeOut(20);
}