//
// Created by bnf on 2021-02-04
//

#ifndef PWLF2021_WORMY_H
#define PWLF2021_WORMY_H

void wormy(int duration);
void grow_worm_and_bounce();
void orientation(int duration);

#endif  // PWLF2021_WORMY_H
