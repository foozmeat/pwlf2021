//
// Created by bnf on 2021-02-05
//

#ifndef PWLF2021_ATOB_H
#define PWLF2021_ATOB_H
#include <stdint.h>

void AtoB(int duration);

uint16_t moveTowards(int A, int B, int increment);
int randomPos();

#endif  // PWLF2021_ATOB_H
