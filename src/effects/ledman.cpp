//
// Created by James Moore on 2/3/21.
//

#include "ledman.h"
#include "lib/shared.h"

typedef struct pill {
    int position;
} pill;

const int num_pills = 60;
pill pills[num_pills];

typedef struct character {
    uint16_t hue{};
    float p{};
    float v{};
    bool dead{};
    bool edible{};
} character;

const character CHARACTER_DEFAULT = {
    .v = -0.05,
};

const int powerpill_offset = 4;
const int pp_pos = powerpill_offset;
bool paused;
bool pill_eaten;
bool draw_score;

character blinky;
character pinky;
character inky;
character clyde;
character lman;

void draw_powerpill() {
    int brightness = beatsin8(240, 0, 255);
    strip.setPixelColor(pp_pos, strip.Color(0, 0, 0, brightness));
}

bool in_bounds(character c) {
    int i_pos = round(c.p);
    return (i_pos >= EAST_END && i_pos < SOUTH_END);
}

void draw_ledman() {

    if (draw_score) return;

    int i_pos = round(lman.p);

    if (in_bounds(lman)) {
        strip.setPixelColor(i_pos, gamma_hsv(lman.hue));
    }

    if (lman.p < 0 && !blinky.dead && !inky.dead && !pinky.dead && !clyde.dead) {
        // make sure ledman wraps around the first time
        lman.p = SOUTH_END;
    }
}

void draw_ghost(character* ghost) {
    int g_pos = round(ghost->p);
    int l_pos = round(lman.p);

    if (in_bounds(*ghost)) {

        if (pill_eaten && !ghost->edible) {
            // turn ghosts blue
            ghost->edible = true;
            ghost->v *= -1.0;
            ghost->hue = BLUE;
//        } else if (round(ghost->p) == CENTER - 15 && ghost->edible && !ghost->dead) {
//             reverse blue ghost when they get near the corner
//            ghost->v *= -1.0;
        }

        if (g_pos == l_pos && !ghost->dead) {
            // ghost is eaten
            ghost->dead = true;
            ghost->v *= 1.8;
            paused = true;
            draw_score = true;
            strip.setPixelColor(l_pos, gamma_hsv(AQUA));

        } else if (ghost->dead) {
            strip.setPixelColor(g_pos, strip.Color(0,0,0, 200));
        } else {
            strip.setPixelColor(g_pos, gamma_hsv(ghost->hue));
        }

    }

}

void draw_pills() {
    for (int i = 0; i < num_pills; i++) {

        if (round(lman.p) == pills[i].position) {
            pills[i].position = -1;
        }

        if (pills[i].position >= EAST_END && pills[i].position < SOUTH_END) {
            strip.setPixelColor(
                pills[i].position, 
                strip.gamma32(strip.Color(0, 0, 0, 100))
            );
        }
    }

}

void update_positions() {
    lman.p += lman.v;
    blinky.p += blinky.v;
    inky.p += inky.v;
    pinky.p += pinky.v;
    clyde.p += clyde.v;
}

void ledman() {

    // reset positions on subsequent runs
    lman = CHARACTER_DEFAULT;
    lman.hue = YELLOW;
    lman.p = EAST_START;

    blinky = CHARACTER_DEFAULT;
    blinky.hue = RED;
    blinky.p = lman.p + 6;

    pinky = CHARACTER_DEFAULT;
    pinky.hue = PINK;
    pinky.p = lman.p + 8;

    inky = CHARACTER_DEFAULT;
    inky.hue = AQUA;
    inky.p = lman.p + 10;

    clyde = CHARACTER_DEFAULT;
    clyde.hue = ORANGE;
    clyde.p = lman.p + 12;


    for (int i = 0; i < num_pills; i++) {
        pills[i].position = 2 * i;
    }

    paused = true;
    pill_eaten = false;
    draw_score = false;
    set_first_leds();

    // draw dots
    draw_pills();

    updateTimers();
    resetStopwatch();
    while (stopwatch < 1000) {
        updateTimers();
        draw_powerpill();
        draw_ledman();
        draw_ghost(&blinky);
        draw_ghost(&pinky);
        draw_ghost(&inky);
        draw_ghost(&clyde);
        strip.show();
    }

    paused = false;

    // ledman and ghosts move towards pill
    while (lman.p >= pp_pos) {
        set_first_leds();
        draw_pills();
        draw_powerpill();
        draw_ghost(&blinky);
        draw_ghost(&pinky);
        draw_ghost(&inky);
        draw_ghost(&clyde);
        draw_ledman();
        strip.show();
        update_positions();
    }

    // ledman eats power pill, ghosts turn blue
    pill_eaten = true;
//    lman.v *= 1.25;

    while (lman.p >= 0 ||
           blinky.p < SOUTH_END ||
           inky.p < SOUTH_END ||
           pinky.p < SOUTH_END ||
           clyde.p < SOUTH_END) {


        set_first_leds();
        draw_pills();
        // draw ghosts in reverse order so eyes appear
        draw_ghost(&blinky);
        draw_ghost(&pinky);
        draw_ghost(&inky);
        draw_ghost(&clyde);
        draw_ledman();
        strip.show();
        update_positions();

        if (paused) {
            paused = false;
            draw_score = false;
            delay(400);
        }
    }

    fadeOut(20);

}
