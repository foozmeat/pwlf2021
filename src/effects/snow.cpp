//
// Created by James Moore on 1/26/21.
//

#include "snow.h"
#include "lib/shared.h"

const int FRAMES_PER_SECOND = 40;
const float fadeAmount = 0.95;
const int fadedBrightness = 50;

int flakeTime = 0;

void snow(int duration) {

    set_first_leds();

    resetStopwatch();

    while (stopwatch < duration * 1000) {
        updateTimers();
        flakeTime += dt;

        for (int i = EAST_END; i < SOUTH_END; i++) {
            uint32_t color = fadeToBlackBy(strip.getPixelColor(i), fadeAmount);
            strip.setPixelColor(i, color);
        }

        if (flakeTime > 100) {
            flakeTime = 0;
            int new_pos = random(1, SOUTH_END - 1);

            if (new_pos > EAST_END) {
                strip.setPixelColor(new_pos - 1, strip.Color(fadedBrightness, fadedBrightness, fadedBrightness, 0));
                strip.setPixelColor(new_pos, strip.Color(fadedBrightness + 25, fadedBrightness + 25, fadedBrightness + 25, 0));
                strip.setPixelColor(new_pos + 1, strip.Color(fadedBrightness,fadedBrightness,fadedBrightness,0));
            }
        }

        strip.show();
        delay(1000 / FRAMES_PER_SECOND);

    }

    fadeOut(20);

}
