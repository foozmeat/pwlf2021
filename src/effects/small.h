//
// Created by James Moore on 2/1/21.
//

#ifndef PWLF2021_SMALL_H
#define PWLF2021_SMALL_H

void confetti(int duration);
void juggle(int duration);
void rainbow_chase(int duration);
void rainbow_waves(int duration);

#endif //PWLF2021_SMALL_H
