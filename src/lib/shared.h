//
// Created by James Moore on 1/19/21.
//

#ifndef PWLF2021_SHARED_H
#define PWLF2021_SHARED_H

#include <Adafruit_NeoPixel.h>
#include <FastLED.h>

#include "easing.h"

#define _NUM_LEDS 120

// These hues are using units of LEDs when displaying a rainbow. It's easier to visully calibrate that way.
#define RED 0
#define ORANGE 10 * 65535 / NUM_LEDS
#define YELLOW 15 * 65535 / NUM_LEDS
#define GREEN 30 * 65535 / NUM_LEDS
#define AQUA 62 * 65535 / NUM_LEDS
#define BLUE 69 * 65535 / NUM_LEDS
#define PURPLE 91 * 65535 / NUM_LEDS
#define PINK 110 * 65535 / NUM_LEDS

extern const int EAST_START;
extern const int EAST_END;
extern const int CENTER;
extern const int SOUTH_START;
extern const int SOUTH_END;
extern const int NUM_LEDS;
extern const int LAST_INDEX;
extern const int DATA_PIN;
extern const int BRIGHTNESS;

extern int ct;
extern int lt;
extern int dt;
extern int stopwatch;

extern Adafruit_NeoPixel strip;

void updateTimers();
void resetStopwatch();
void fadeOut(int delay_ms);
float float_rand( float min, float max );
uint16_t wrap16(uint16_t in);
uint32_t gamma_hsv(uint16_t hue);
uint32_t fadeToBlackBy(uint32_t color, float fadefactor);
uint32_t addColors(uint32_t color1, uint32_t color2);
uint32_t CRGBToColor(CRGB c);
CRGB colorToCRGB(uint32_t color);
void set_first_leds();

#endif //PWLF2021_SHARED_H
