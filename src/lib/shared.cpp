//
// Created by James Moore on 1/19/21.
//

#include "shared.h"

void updateTimers() {
    ct = millis();
    dt = ct - lt;

    if (dt < 0) {
        dt = 0;
    }

    lt = ct;

    stopwatch += dt;

//    Serial.printf("CT: %i\tDT: %i\tSW: %i\n", ct, dt, stopwatch);
}

void resetStopwatch() {
    stopwatch = 0;
}

void fadeOut(int delay_ms) {

    for (int f = 0; f < 64; f++) {
        uint8_t b = strip.getBrightness();
        uint8_t new_b = max(0, b - 4);
        strip.setBrightness(new_b);
        strip.show();
        delay(delay_ms);
    }
    strip.clear();
    strip.show();
    delay(1000);
    strip.setBrightness(BRIGHTNESS);
}

float float_rand( float min, float max ) {
    float scale = random() / (float) RAND_MAX; /* [0, 1.0] */
    return min + scale * ( max - min );      /* [min, max] */
}

uint16_t wrap16(uint16_t in) {
    if (in > 65535) {
        return in - 65535;
    } else {
        return in;
    }
}

uint32_t gamma_hsv(uint16_t hue) {
    return strip.gamma32(strip.ColorHSV(hue, 255, 255));
}

CRGB colorToCRGB(uint32_t color) {
    // uint8_t w = (uint8_t)(color >> 24);
    uint8_t r = (uint8_t)(color >> 16);
    uint8_t g = (uint8_t)(color >> 8);
    uint8_t b = (uint8_t)(color >> 0);

    return CRGB(r, g, b);
}

uint32_t CRGBToColor(CRGB c) {
    return strip.Color(c.r, c.g, c.b);
}

uint32_t addColors(uint32_t color1, uint32_t color2) {
    CRGB c1 = colorToCRGB(color1);
    CRGB c2 = colorToCRGB(color2);

    CRGB newColor = c1 + c2;

    return CRGBToColor(newColor);
}

uint32_t fadeToBlackBy(uint32_t color, float fadefactor) {

    if (color == 0) {
        return 0;
    }

    CRGB c = colorToCRGB(color);
    uint8_t w = (uint8_t)(color >> 24);

    // printf("r: %i\tg: %i\tb: %i -> ", c.r, c.g, c.b);
    w *= fadefactor;
    c.r *= fadefactor;
    c.g *= fadefactor;
    c.b *= fadefactor;
    // printf("r: %i\tg: %i\tb: %i\n", c.r, c.g, c.b);

    uint32_t new_color = strip.Color(c.r, c.g, c.b, w);
    // printf("color: %lu\tnew color: %lu\n", color, new_color);

    // Return the new color
    return new_color;
}

void set_first_leds() {
    strip.clear();
    if (SOUTH_END < LAST_INDEX) {
        strip.fill(strip.Color(255, 255, 255, 0), SOUTH_END);
        // strip.fill(strip.Color(0, 0, 0, 255), SOUTH_END);
    }
    // don't run show here or there will be big slowdowns elsewhere
    // strip.show();
}