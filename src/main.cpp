#include "effects/AtoB.h"
#include "effects/fireworks.h"
#include "effects/ledman.h"
#include "effects/planets.h"
#include "effects/rgb.h"
#include "effects/small.h"
#include "effects/wormy.h"
#include "effects/blocks.h"
#include "effects/countdown.h"
#include "effects/fire.h"
#include "effects/meteor.h"
#include "effects/pacifica.h"
#include "effects/pendulum.h"
#include "effects/snow.h"
#include "effects/chase.h"


#include "lib/shared.h"
#include "lib/trng.h"

// Test strip is 144 LEDS SK6812 RGBW
// Production strip is 120 LEDS USC2904 (WS2812) GRBW

const int NUM_LEDS = _NUM_LEDS;
const int LAST_INDEX = _NUM_LEDS - 1;

const int EAST_END = 0;
const int EAST_START = 72;
const int CENTER = 73;
const int SOUTH_START = 74;
const int SOUTH_END = 117; // set to NUM_LEDS to use the whole strip

const int DATA_PIN = 10;

const int BRIGHTNESS = 100;
int ct = 0;
int lt = 0;
int dt = 0;
int stopwatch = 0;

#ifdef DEV
#define NEO_PTYPE NEO_GRBW // Testing
#else
#define NEO_PTYPE NEO_RGBW  // Production
#endif

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, DATA_PIN, NEO_PTYPE);

void setup() {
    Serial.begin(115200);
    // while (!Serial); // wait for serial connection before starting
    strip.begin();
    strip.setBrightness(BRIGHTNESS);
    strip.clear();
    delay(1000);

    trng_init();
    uint16_t seed = trng() % 65536;
    random16_set_seed(seed);
    random16_add_entropy(analogRead(3));

    ct = millis();
    lt = ct;
}
int p = 0;

void loop() {

    strip.clear();

    strip.setPixelColor(0, 255,255,255, 255);
    strip.setPixelColor(1, 255,255,255, 255);
    strip.setPixelColor(2, 255,255,255, 255);
    strip.show();


    // strip.fill(strip.ColorHSV(31000, 204, 10));
    // strip.fill(strip.Color(2, 6, 10));


    // strip.setPixelColor(p++, gamma_hsv(RED));
    // strip.setPixelColor(p++, gamma_hsv(ORANGE));
    // strip.setPixelColor(p++, gamma_hsv(YELLOW));
    // strip.setPixelColor(p++, gamma_hsv(GREEN));
    // strip.setPixelColor(p++, gamma_hsv(AQUA));
    // strip.setPixelColor(p++, gamma_hsv(BLUE));
    // strip.setPixelColor(p++, gamma_hsv(PURPLE));
    // strip.setPixelColor(p++, gamma_hsv(PINK));

    // Serial.printf("%lu\t%lu\t%lu\t%lu\t%lu\t%lu\t%lu\t%lu\n",
    //                 RED,
    //                 ORANGE,
    //                 YELLOW,
    //                 GREEN,
    //                 AQUA,
    //                 BLUE,
    //                 PURPLE,
    //                 PINK);

    // strip.rainbow(0);
    // strip.show();
    // exit(0);

    countdown(15);
    planets(20);    // seconds
    color_blocks(20);
    meteorRain(5);  // loops
    Fire(15);       // seconds
    pacifica(20);
    AtoB(15);
    rainbow_chase(20);
    juggle(15);     // seconds
    pendulum();
    rainbow_waves(20);
    ledman();
    confetti(20);  // seconds
    wormy(20);
    snow(20);
}
